/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 88.25688073394495, "KoPercent": 11.743119266055047};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.0, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.0, 500, 1500, "Go to Benefits Screen"], "isController": false}, {"data": [0.0, 500, 1500, "Go to Benefits Screen-2"], "isController": false}, {"data": [0.0, 500, 1500, "Auth"], "isController": false}, {"data": [0.0, 500, 1500, "Go to Benefits Screen-1"], "isController": false}, {"data": [0.0, 500, 1500, "Go to Benefits Screen-0"], "isController": false}, {"data": [0.0, 500, 1500, "Login Request"], "isController": false}, {"data": [0.0, 500, 1500, "List, Update Benefits"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 13625, 1600, 11.743119266055047, 54469.753541284525, 1512, 114399, 53143.0, 106431.2, 111445.0, 113689.0, 39.38373487881372, 128.2662219661878, 9.196336386777183], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["Go to Benefits Screen", 2000, 267, 13.35, 95692.35900000001, 31827, 114399, 101844.5, 113462.9, 113799.0, 114320.99, 13.82208215845635, 97.329770799642, 9.004938046836125], "isController": false}, {"data": ["Go to Benefits Screen-2", 1875, 142, 7.573333333333333, 34048.561066666756, 11947, 44211, 35678.0, 40814.4, 42235.2, 43513.0, 16.805291650234825, 93.30635780594594, 3.1345807667918475], "isController": false}, {"data": ["Auth", 2000, 171, 8.55, 49495.66699999994, 31582, 62954, 51380.0, 62912.0, 62926.0, 62940.0, 21.434389334247868, 18.594426941553778, 0.0], "isController": false}, {"data": ["Go to Benefits Screen-1", 1875, 0, 0.0, 15651.342933333346, 1512, 35309, 12946.0, 30630.800000000003, 33774.0, 34909.88, 18.560864787812193, 18.705871543966975, 3.4620363031954384], "isController": false}, {"data": ["Go to Benefits Screen-0", 1875, 0, 0.0, 48722.725866666726, 31587, 71641, 42434.0, 65410.4, 65428.0, 71635.0, 18.39605980926965, 16.67142920215062, 5.551154766664377], "isController": false}, {"data": ["Login Request", 2000, 94, 4.7, 48464.24249999998, 31708, 62595, 51083.5, 62447.0, 62545.95, 62584.0, 21.269581308291947, 122.6780637834863, 3.385685305909753], "isController": false}, {"data": ["List, Update Benefits", 2000, 926, 46.3, 85151.7120000001, 59762, 107391, 75115.0, 107228.0, 107278.0, 107374.0, 18.605689619885762, 28.106855818696857, 2.743612434182373], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["502/Bad Gateway", 590, 36.875, 4.330275229357798], "isController": false}, {"data": ["504/Gateway Time-out", 920, 57.5, 6.752293577981652], "isController": false}, {"data": ["500/HTTP/1.1 500", 90, 5.625, 0.6605504587155964], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 13625, 1600, "504/Gateway Time-out", 920, "502/Bad Gateway", 590, "500/HTTP/1.1 500", 90, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": ["Go to Benefits Screen", 2000, 267, "502/Bad Gateway", 267, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["Go to Benefits Screen-2", 1875, 142, "502/Bad Gateway", 142, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["Auth", 2000, 171, "500/HTTP/1.1 500", 90, "502/Bad Gateway", 81, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["Login Request", 2000, 94, "502/Bad Gateway", 94, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["List, Update Benefits", 2000, 926, "504/Gateway Time-out", 920, "502/Bad Gateway", 6, null, null, null, null, null, null], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
